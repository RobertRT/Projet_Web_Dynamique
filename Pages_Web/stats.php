<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <title>Saisie</title>
   </head>
   <body>
      <table>
         <h1>Statistiques des Absences</h1>
         <tr>
            <th>NOM</th>
            <th>PRENOM</th>
            <th>NUM E</th>
            <th> ABSENCE UE11 EN %</th>
         </tr>
         <?php
            $bdd= new PDO('mysql:host=localhost;dbname=projetweb;charset=utf8','root','');
            
            
              $reponse=$bdd->query( 'SELECT absence.NumE,liste_etudiants.PrenomE,liste_etudiants.NomE, (SUM(Duree))*100/315 AS Pourcentage_absence_UE11 FROM absence,liste_etudiants WHERE absence.NumE=liste_etudiants.NumE and NumUE=11 GROUP By NumE');
            
               while($donnees=$reponse->fetch()){  ?>
         <tr>
            <td><?php echo $donnees['NomE'];?></td>
            <td><?php echo $donnees['PrenomE'];?></td>
            <td><?php echo $donnees['NumE'];?></td>
             <?php if($donnees['Pourcentage_absence_UE11']<'9')
               {?>
            <td><?php echo $donnees['Pourcentage_absence_UE11'];?></td>
         </tr>
          <?php
               }
        
        ?>
          <?php if($donnees['Pourcentage_absence_UE11']>'9')
               {?>
            <td><?php echo'<font color="red">'. $donnees['Pourcentage_absence_UE11'].'</font>';?></td>
         </tr>
          <?php
               }
        
        ?>
         <?php        
            }
            $reponse->closeCursor();
            ?>
      </table>
      <br/>
      <table>
         <tr>
            <th>NOM</th>
            <th>PRENOM</th>
            <th>NUM E </th>
            <th>ABSENCE UE12 EN %</th>
         </tr>
         <?php
            $reponse=$bdd->query( 'SELECT absence.NumE,liste_etudiants.PrenomE,liste_etudiants.NomE, (SUM(Duree))*100/225 AS Pourcentage_absence_UE12 FROM absence,liste_etudiants WHERE absence.NumE=liste_etudiants.NumE and NumUE=12 GROUP By NumE');
            
             while($donnees=$reponse->fetch()){  ?>
         <tr>
            <td><?php echo $donnees['NomE'];?></td>
            <td><?php echo $donnees['PrenomE'];?></td>
            <td><?php echo $donnees['NumE'];?></td>
                 <?php if($donnees['Pourcentage_absence_UE12']<'9')
               {?>
            <td><?php echo $donnees['Pourcentage_absence_UE12'];?></td>
         </tr>
          <?php
               }
        
        ?>
          <?php if($donnees['Pourcentage_absence_UE12']>'9')
               {?>
            <td><?php echo'<font color="red">'. $donnees['Pourcentage_absence_UE12'].'</font>';?></td>
         </tr>
          <?php
               }
        
        ?>
         </tr>
         <?php        
            }
            $reponse->closeCursor();
            ?>
      </table>