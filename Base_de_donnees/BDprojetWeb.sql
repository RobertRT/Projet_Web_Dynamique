-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mar 06 Juin 2017 à 10:09
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetweb`
--

-- --------------------------------------------------------

--
-- Structure de la table `absence`
--

CREATE TABLE `absence` (
  `dateA` date NOT NULL,
  `Duree` int(11) DEFAULT NULL,
  `NumE` int(11) DEFAULT NULL,
  `NumUE` int(11) DEFAULT NULL,
  `NumA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `absence`
--

INSERT INTO `absence` (`dateA`, `Duree`, `NumE`, `NumUE`, `NumA`) VALUES
('2017-06-06', 2, 36001808, 12, 103),
('2017-06-06', 2, 36001857, 12, 104),
('2017-06-07', 2, 36001808, 12, 105),
('2017-06-07', 2, 36001857, 12, 106),
('2017-06-09', 3, 36001857, 11, 107),
('2017-06-10', 3, 36001523, 12, 108),
('2017-06-06', 1, 36001933, 11, 109),
('2017-06-08', 3, 35006269, 12, 110),
('2017-06-07', 3, 36002054, 11, 111),
('2017-06-12', 2, 36001523, 12, 112),
('2017-06-08', 2, 36001523, 11, 113);

-- --------------------------------------------------------

--
-- Structure de la table `liste_etudiants`
--

CREATE TABLE `liste_etudiants` (
  `NumE` int(11) NOT NULL,
  `NomE` varchar(25) DEFAULT NULL,
  `PrenomE` varchar(25) DEFAULT NULL,
  `TP` varchar(11) DEFAULT NULL,
  `Departement` varchar(25) DEFAULT NULL,
  `TD` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `liste_etudiants`
--

INSERT INTO `liste_etudiants` (`NumE`, `NomE`, `PrenomE`, `TP`, `Departement`, `TD`) VALUES
(35000656, 'ADENOR', 'Bryan', 'TP1', 'RT1', 'TD1'),
(35006269, 'AHO', 'Mathieu', 'TP3', 'RT1', 'TD2'),
(36001523, 'Lebeau', 'Paco', 'TP2', 'RT1', 'TD2'),
(36001808, 'BERNARD', 'Fabien', 'TP1', 'RT1', 'TD1'),
(36001857, 'BACO', 'Sabrina', 'TP1', 'RT1', 'TD1'),
(36001933, 'ALIDOR', 'Ludovic', 'TP1', 'RT1', 'TD1'),
(36002054, 'AUDIGIER', 'Mael', 'TP1', 'RT1', 'TD1'),
(36006017, 'BABET', 'Florent', 'TP1', 'RT1', 'TD1');

-- --------------------------------------------------------

--
-- Structure de la table `modules`
--

CREATE TABLE `modules` (
  `NumM` varchar(11) NOT NULL,
  `NomM` varchar(25) DEFAULT NULL,
  `NumUE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `modules`
--

INSERT INTO `modules` (`NumM`, `NomM`, `NumUE`) VALUES
('1101', 'Init° Res entreprise', 11),
('1102', 'Init° telephonie', 11),
('1103', 'Archit equipement info', 11),
('1104', 'Princ. et archit. Res', 11),
('1105', 'Base SE', 11),
('1106', 'Init° Dvpment Web', 11),
('1107', 'Init° Mesure signal', 11),
('1108', 'Acquisit° codage info', 11),
('1109', 'PT', 11),
('1201', 'Anglais general', 12),
('1202', 'EC', 12),
('1203', 'PPP', 12),
('1204', 'Numerat°/Caclus', 12),
('1205', 'Outils signal', 12),
('1206', 'Circuits elec', 12),
('1207', 'Base de la prog', 12),
('1208', 'Soutient Math', 12);

-- --------------------------------------------------------

--
-- Structure de la table `ue`
--

CREATE TABLE `ue` (
  `NumUE` int(11) NOT NULL,
  `NomUE` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ue`
--

INSERT INTO `ue` (`NumUE`, `NomUE`) VALUES
(11, 'Decouvertes metiers'),
(12, 'MN competence tran/scien'),
(21, 'Consolidation metier'),
(22, 'Dv competence tran/scien');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `absence`
--
ALTER TABLE `absence`
  ADD PRIMARY KEY (`NumA`),
  ADD KEY `FK_Absence_NumE` (`NumE`),
  ADD KEY `FK_Absence_NumUE` (`NumUE`);

--
-- Index pour la table `liste_etudiants`
--
ALTER TABLE `liste_etudiants`
  ADD PRIMARY KEY (`NumE`);

--
-- Index pour la table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`NumM`),
  ADD KEY `FK_Modules_NumUE` (`NumUE`);

--
-- Index pour la table `ue`
--
ALTER TABLE `ue`
  ADD PRIMARY KEY (`NumUE`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `absence`
--
ALTER TABLE `absence`
  MODIFY `NumA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `absence`
--
ALTER TABLE `absence`
  ADD CONSTRAINT `FK_Absence_NumE` FOREIGN KEY (`NumE`) REFERENCES `liste_etudiants` (`NumE`),
  ADD CONSTRAINT `FK_Absence_NumUE` FOREIGN KEY (`NumUE`) REFERENCES `ue` (`NumUE`);

--
-- Contraintes pour la table `modules`
--
ALTER TABLE `modules`
  ADD CONSTRAINT `FK_Modules_NumUE` FOREIGN KEY (`NumUE`) REFERENCES `ue` (`NumUE`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
